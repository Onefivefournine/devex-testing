# Devex Test
## Requirements
- Node js ^8.x
- MongoDB

## Usage
- Build the app with `yarn build` or `npm run build`
- Fill `server/config.js` and `src/config.js` with your data if needed
- Serve it with `node server`
- Go to [http://localhost:8888](http://localhost:8888)
