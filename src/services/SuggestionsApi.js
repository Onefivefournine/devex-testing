import config from '../config'

class SuggestionsApi {
    constructor( baseUrl ) {
        this.baseUrl = baseUrl
    }
    async getAddress( query ) {
        let lang = ( navigator.languages && navigator.languages.length ) ? navigator.languages[ 0 ] : navigator.language
        let res = await fetch( `${this.baseUrl}/suggestions/address?q=${query}&lang=${lang}` )
        return res.json()
    }
}

export default new SuggestionsApi( config.API_BASE_URL )