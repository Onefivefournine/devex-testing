const mongoose = require( 'mongoose' );

const LogEntrySchema = new mongoose.Schema( {
    query: { type: String, required: true },
    lang: { type: String, required: true },
    ip: { type: String, required: true },
    result: { type: Object, required: true },
    created_at: { type: Date, default: new Date() },
} );

const LogEntry = mongoose.model( 'LogEntry', LogEntrySchema );

module.exports = { LogEntry, LogEntrySchema };