const express = require( 'express' );
const mongoose = require( 'mongoose' );
const cors = require( 'cors' ); // for dev frontend server
const path = require( 'path' );
const config = require( './config' );
const SuggestionsController = require( './controllers/SuggestionsController' );

const app = express();
app.use( cors() ); // for dev frontend server
app.use( express.static( path.resolve( __dirname, '../dist' ) ) );

const port = config.APP_PORT;

mongoose.Promise = global.Promise;

mongoose.connect( config.DATABASE_URL, { promiseLibrary: global.Promise, useNewUrlParser: true } )
    .then( () => {
        app.get( '/suggestions/address', SuggestionsController.init.bind( SuggestionsController ) )
        app.get( '*', ( req, res ) => {
            res.sendFile( path.resolve( __dirname, '../dist/index.html' ) );
        } );

        app.listen( port, () => {
            console.log( 'Server is listening on http://localhost:' + port );
        } );
    } )
    .catch( ( err ) => {
        console.error( err )
        throw new Error( err )
    } )