module.exports = {
    DATABASE_URL: 'mongodb://localhost:27017/testbase',
    SUGGESTIONS_URL: 'https://maps.googleapis.com/maps/api/place/autocomplete/json?key=AIzaSyCRPriHiZAd-Bykb4pir3G68lKHkZaNiX4',
    APP_PORT: 8888
}