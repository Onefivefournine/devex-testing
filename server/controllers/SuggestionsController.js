const { LogEntry } = require( '../models/LogEntry' );
const axios = require( 'axios' );
const config = require( '../config' );
class SuggestionsController {
    static async init( req, res ) {
        if ( !req.query || !req.query.q ) {
            res.status( 400 ).json( { 'error': 'No "q" parameter found!' } )
        }

        try {
            let predictions = await this._getPredictions( req )

            res.send( predictions.map( p => p.description ) )
        } catch ( error ) {
            console.error( error )
            res.status( 400 ).json( { 'error': `Suggestion failure! ${error.message}` } )
        }
    }

    static async _getPredictions( req ) {
        let result = []
        try {
            let d = new Date()
            d.setDate( d.getDate() - 1 )
            let found = await LogEntry.findOne( {
                query: req.query.q,
                lang: req.query.lang,
                ip: req.connection.remoteAddress,
                created_at: { $gt: d }
            }, 'result' )

            result = found && found.result
        } catch ( error ) {
            console.error( error )
        }

        if ( !result ) {
            try {
                let resp = await axios.get(
                    `${config.SUGGESTIONS_URL}&language=${encodeURIComponent( req.query.lang || 'en' )}&input=${encodeURIComponent( req.query.q )}`
                )

                LogEntry.create( {
                    query: req.query.q,
                    lang: req.query.lang,
                    ip: req.connection.remoteAddress,
                    result: resp.data.predictions
                } )

                switch ( resp.data.status ) {
                    case 'OK':
                        result = resp.data.predictions
                        break;
                    case 'OVER_QUERY_LIMIT':
                    case 'REQUEST_DENIED':
                    case 'INVALID_REQUEST':
                    case 'UNKNOWN_ERROR':
                        throw new Error( resp.data.status )
                    case 'ZERO_RESULTS':
                    default:
                        break;
                }
            } catch ( error ) {
                console.error( error )
                throw new Error( error )
            }
        }

        return result || []
    }
}

module.exports = SuggestionsController