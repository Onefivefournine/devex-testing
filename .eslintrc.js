module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: [ 'plugin:vue/essential', '@vue/standard', 'eslint:recommended' ],

  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    indent: 'off',
    'space-before-function-paren': 'off',
    'one-var': 0,
    quotes: [ 'warn', 'single' ],
  },
  parserOptions: {
    parser: 'babel-eslint'
  }
}
